const mongoose = require('mongoose');
const config = require('config');
const db = config.get('mongoURI');
 
const connectDb = async () => {
 
	try{
		console.log("Mongo Db  before connection.......");
		await mongoose.connect("mongodb+srv://prabin123:prabin123@developernw-x78pm.mongodb.net/test?retryWrites=true&w=majority",
 { useNewUrlParser: true ,useCreateIndex: true ,useFindAndModify :false} ,() => { })
        console.log("Mongo Db after connection....");
	}catch(err){
         console.log(err.message);
         //Exit process with failure
         process.exit(1);
	}
};

 
module.exports = connectDb;